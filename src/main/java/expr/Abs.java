package expr;

class Abs extends Expr {
    private double value;

    Abs(double value) {
        this.value = value;
    }

    public String toString(int prec) {
        return String.valueOf(Math.abs(value));
    }

    public double value(Environment env) {
        return Math.abs(value);
        //dasdasdasda
    }
}